class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery #with: :exception
  before_filter :need_login, unless: -> {
  	controller_name == 'sessions'
  }
  protected
  def need_login
  	unless already_logged_in?
  		session[:requested_url] = request.url
  		redirect_to new_sessions_path 
  end
end  
  	def already_logged_in?
  		!session[:user_id].nil?  			
  		end	
  	end